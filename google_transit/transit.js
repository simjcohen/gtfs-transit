var express = require('express');
const {exec} = require('child_process');
var app = express();

var __public_dir = "/home/simon/go/src/transit/google_transit/public"

app.get('/', function (req, res) {
  res.sendFile("index.html", {root: __public_dir});
});
app.get('/stats', (req, res) => {
    exec('python3 gpu_graph.py', (err, stdout, stderr) => {
        if (err) {
            res.sendStatus(500)
            return
        }
        res.send(stdout)
    });
});
app.listen(3333, function () {
  console.log('Begin listener on 3333');
});

