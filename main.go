package main

import (
    "fmt"
    "strings"
	"bytes"
    "strconv"
    "errors"
    "time"
	"os"
    "net/http"
    "google.golang.org/protobuf/proto"
    "io/ioutil"
    "bufio"

	//"database/sql"
	_ "github.com/mattn/go-sqlite3"

)



import pb "transit/transit_proto"
import wgs "github.com/lvdlvd/go-geo-wgs84"
import "math"

const walking_dist = 500.0

var stop_descs map[string]string

var nyc_loc *time.Location
var trips map[string]*Trip
var stations map[string]*Station
var transfers map[string][]Transfer

type Transfer struct {
	origin string;
	dest   string;
	time   int;
	mode   uint8;
}

var s, e time.Time
func main() {
    s = time.Now()
    nyc_loc, _ = time.LoadLocation("America/New_York")

	stop_descs = make(map[string]string)

    /*
	db, err := sql.Open("sqlite3", "nodes.db")
	if err != nil {
		fmt.Printf("sql.Open: %v\n", err)
		return
	}
	defer db.Close()

	_, err = db.Exec(`CREATE TABLE IF NOT EXISTS nodes (
		g string, l string, e string
	)`)
	if err != nil {
		fmt.Printf("sql.Exec: %v\n", err)
		return
	}
    */

	// build stop description map
	txt, err := os.ReadFile("google_transit/stops.txt")
	if err != nil {
        fmt.Printf("%v", err)
        return
    }

	for _, line := range bytes.Split(txt, []byte("\n")) {
		cols := bytes.Split(line, []byte(","))
		if len(cols) < 2 {
			continue
		}
		stop_descs[string(cols[0])] = string(cols[1])
	}

    // build list of in- and out- of system transfers
	transfers = make(map[string][]Transfer)
	txt, err = os.ReadFile("google_transit/transfers.txt")
	if err != nil {
        fmt.Printf("%v", err)
        return
    }

	for ix, line := range strings.Split(string(txt), "\n") {
		// skip header
		if ix == 0 { continue }

		cols := strings.Split(line, ",")
		if len(cols) !=  4 {
			continue
		}
		origin := cols[0]
		dest := cols[1]
		if origin == dest { continue }
		mode, _ := strconv.Atoi(cols[2])
		transit_time, _ := strconv.Atoi(cols[3])

		t := Transfer{origin, dest, transit_time, uint8(mode)}
		transfers[t.origin] = append(transfers[t.origin], t)
	}

	txt, err = os.ReadFile("google_transit/stops.txt")
	if err != nil {
        fmt.Printf("%v", err)
        return
    }

    // Now out-of-system, rough guesses based on coords
    locations := make(map[string][]float64)
	for ix, line := range strings.Split(string(txt), "\n") {
        if ix == 0 { continue }

		cols := strings.Split(line, ",")
		if len(cols) <  4 { continue }
        lat, _ := strconv.ParseFloat(cols[2], 64)
        lon, _ := strconv.ParseFloat(cols[3], 64)
        locations[cols[0][:3]] = []float64{lat, lon}
    }
    for s1, l1 := range locations {
        for s2, l2 := range locations {
            if s1 == s2 { continue }
            const walking_speed = 1.31
            if d := coords2dist(l1[0], l1[1], l2[0], l2[1]); d < walking_dist {
                //fmt.Printf("Out of system walking transfer possible between %s (%s) and %s (%s) (%f meters, %0.2f s)\n",
                //    stop_descs[s1], s1, stop_descs[s2], s2, d, d / walking_speed)
                t := Transfer{s1, s2, int(d / walking_speed), 255}
                transfers[s1] = append(transfers[s1], t)
            }
        }
    }
    e = time.Now()
    fmt.Printf("%f seconds to load all static data structures\n", e.Sub(s).Seconds())

	trips = make(map[string]*Trip)
	stations = make(map[string]*Station)
    s = time.Now()
    get_trains_by_line("-g")
    get_trains_by_line("-ace")
    get_trains_by_line("-l")
    get_trains_by_line("-bdfm")
    get_trains_by_line("")
    e = time.Now()
    fmt.Printf("%f seconds to parse all trains\n", e.Sub(s).Seconds())

	//get_next_to_arrive("A28", "N")
    reader := bufio.NewReader(os.Stdin)
    txt, err = os.ReadFile("google_transit/stops.txt")
    for ;; {
        fmt.Print("Enter a destination: ")
        input, err := reader.ReadString('\n')
        if err != nil {
            fmt.Println("file closed unexpectedly")
            return
        }
        stops := make([]string, 0)
        for _, l := range strings.Split(string(txt), "\n") {
            if len(l) <= 4 || l[3] != ',' { continue }
            if _, ok := stations[l[:3]]; !ok { continue }
            cols := strings.Split(strings.ToLower(l), ",")
            if len(cols) < 2 { continue}
            d := cols[1]
            if strings.Contains(d, strings.Trim(strings.ToLower(input), "\n")) {
                stops = append(stops, l[:3])
            }
        }
        if len(stops) == 0 {
            fmt.Printf("Could not find a match for %s\n",
                    strings.Trim(strings.ToLower(input), "\n")) 
            continue
        } else if len(stops) > 1 {
            for ;; {
                for i, s := range stops {
                    fmt.Printf("%d  %-24s [%s]\n", i, stop_descs[s], s)
                }
                fmt.Print("Enter a destination: ")
                input, err := reader.ReadString('\n')
                if err != nil {
                    fmt.Println("file closed unexpectedly")
                    return
                }
                i, err := strconv.Atoi(strings.Trim(input, "\n"))
                if err != nil || i >= len(stops) {
                    fmt.Println("Enter a number between", 0, "and", len(stops) - 1)
                    continue
                }
                stops[0] = stops[i]
                break
            }
        }
	    navigate("G26", stops[0])
    }
}

type Edge struct {
	trip *Trip;
	origin *Node;
	dest *Node;
}

func transfer_possible(id string) (bool) {

	lines := make(map[string]struct{})
	for _, ntoa := range stations[id].arrivals {
		if len(ntoa.stop.transfers) > 0 { return true }
		lines[ntoa.trip.route_id] = struct{}{}
	}
	return len(lines) > 1
}

type Node struct {
    station *Station
    dist int; // seconds
    time time.Time;
    arr *Arrival;// not tracked by dijkstra
    prev *Node;
}

func New_Node(station string) (*Node) {
    if stations[station] == nil {
        panic("error")
    }
    return &Node{stations[station], 0, time.Unix(0, 0), nil, nil}
}

/* nodes, uninitialized nodes. 
 * Only includes origin, destination, and all transfer stations 
 */
func do_Dijkstra(nodes map[string]*Node, origin, dest string) (map[string]*Node) {
    /* copy nodes to not_visited */
    not_visited := make(map[string]*Node)
    for k,v := range nodes {
        not_visited[k] = v
    }

	for _, node := range nodes {
		node.dist = 1<<32 - 1 /* ~ 120 years */
		node.prev = nil
        node.time = time.Unix(int64(node.dist), 0)
	}
    nodes[origin].dist = 0
    nodes[origin].time = time.Now()
    

    for ; len(not_visited) > 0; {
        /* find u, minimum distance from origin */
        //fmt.Printf("%v\n", not_visited)
        var u string
        min_dist := 1<<32
        for id, n := range not_visited {
            if n.dist < min_dist {
                u = id
                min_dist = n.dist
            }
        }
        //fmt.Printf("Searching %s (%-24s) neighbors...\n", u, stop_descs[u])

        /* search u's next-to-arrive for trains stopping at any other node */
        for _, ntoa := range nodes[u].station.arrivals {
			//fmt.Printf("Found a %-12s bound %s train %s ", stop_descs[ntoa.trip.stops[len(ntoa.trip.stops)-1].id], ntoa.trip.route_id, ntoa.trip.trip_id) 
            var i int
            var s Stop
            var stops []Stop = ntoa.trip.stops
            var start_time int

            /* look at stops only after u */
            for i=0; i<len(stops); i++ {
                s = stops[i]
                if s.id[:3] == ntoa.stop.id[:3] { 
                    start_time = int(s.dep.Unix())
                    break 
                }
            }
			if i + 1 == len(stops) {
				//fmt.Printf("-- at last stop --\n")
			}

            //fmt.Printf("arriving to %s %-12s at %s\n", stops[i].id, stop_descs[stops[i].id], stops[i].arr.Format("15:04:05"))

            /* Don't look at ntoa's that depart before the previous transfer arrives */
            if stops[i].dep.Before(nodes[u].time) { continue }

			for i+=1; i<len(stops); i++ {
                s = ntoa.trip.stops[i]
                /* 
                 * check for wrap around condition
                 * if this train stops at u's station, then we can stop checking.
                 * any other stops would be faster via a train in the opposite
                 * direction
                 */
                if s.id[:3] == ntoa.stop.id[:3] { break } 
                if v, ok := nodes[s.id[:3]]; ok {
                    end_time := int(s.arr.Unix())
                    /*
                    fmt.Printf("%s (%-12s) @ %s --> %s (%-12s) @ %s\n", 
                            u, stop_descs[u], time.Unix(int64(start_time), 0).Format("15:04:05"), v.station.id,
                            stop_descs[v.station.id], time.Unix(int64(end_time), 0).Format("15:04:05"))
                            */
                    //if alt := not_visited[u].dist + (end_time - start_time); alt < v.dist {
                    if alt := not_visited[u].dist + (end_time - start_time); s.arr.Before(v.time) {
                        v.dist = alt
                        v.prev = nodes[u]
                        v.time = s.arr
                        v.arr = &Arrival{ntoa.trip, ntoa.stop}
                        /*
                        fmt.Printf("Faster route found to %s (%-12s) from %s (%-12s) %s-%s (total distance %d)\n",
                                v.station.id, stop_descs[v.station.id], u, stop_descs[u],
                                time.Unix(int64(start_time), 0).Format("15:04:05"), 
                                time.Unix(int64(end_time), 0).Format("15:04:05"), 
                                alt)
                                */

                    }
                }
            }
        }
        /* Also check for foot transfers */
        for _, t := range nodes[u].station.arrivals[0].stop.transfers {
            if v, ok := nodes[t.dest]; ok {
                //if alt := not_visited[u].dist + t.time; alt < v.dist {
                if alt := not_visited[u].dist + t.time;
                    v.time.After(not_visited[u].time.Add(time.Duration(t.time))) {
                    v.dist = alt
                    v.prev = nodes[u]
                    v.time = nodes[u].time.Add(time.Duration(t.time))
                    v.arr = nil
                    /*
                    fmt.Printf("Faster route found to %s (%-12s) from %s (%-12s) walking transfer %d (total distance %d)\n",
                            v.station.id, stop_descs[v.station.id], u, stop_descs[u],
                            t.time, alt)
                            */
                }
            }
        }
        delete(not_visited, u)
    } /* end while Q */
	return nodes
	/* Q = [all transfer stations, origin, destination */
	/*
		Node {
			station *Station
			dist int; // seconds
			prev *Node;
		}
	*/
	/* pop u, the min dist from Q; for trains arriving to u
			if <transfer_node> in ntoa.trip.stops
				v = <transfer_node>
				alt = u.dist + ntoa[v] - ntoa[u]
				if alt < v.dist
					v.dist = alt
					v.prev = u
	*/
}

//func get_nodes(origin string) ([]string) {
func get_nodes(origin string) (map[string][]string) {
    nodes := make(map[string][]string)
	not_visited := make([]string, 1)
	visited := make(map[string]struct{})
	not_visited[0] = origin

	for ; len(not_visited) > 0; not_visited = not_visited[1:] {
		origin = not_visited[0]
        arrivals := make([]Arrival, len(stations[origin].arrivals))
        copy(arrivals, stations[origin].arrivals)
        lines := make(map[string]struct{})
		for _, ntoa := range arrivals {
            lines[ntoa.trip.trip_id[len(ntoa.trip.trip_id)-4:]] = struct{}{}
        }

		/* consider all transfers in next to arrive */
		for _, s := range transfers[origin] {
			if _, ok := stations[s.dest]; ok {
				for _, a := range stations[s.dest].arrivals {
					InsertArrival(&a, &arrivals)
					//fmt.Printf("Transfer: %v %v\n", a.trip, a.stop)
				}
			}
		}
		visited[origin] = struct{}{}
		//fmt.Printf("%v %v\n", visited, not_visited)
		for _, ntoa := range arrivals {
            //fmt.Printf("Found a %-12s bound %s train %s ", stop_descs[ntoa.trip.stops[len(ntoa.trip.stops)-1].id], ntoa.trip.route_id, ntoa.trip.trip_id) 

			i := 0
			var s Stop
			stops := ntoa.trip.stops
			/* set origin in case arrival is a transfer */
			origin = ntoa.stop.id[:3]
			for i, s = range stops {
				if s.id[:3] == origin[:3] { break }
			}
			//start := stops[i]
			if i + 1 == len(stops) {
				//fmt.Printf("-- at last stop --\n")
			}
			//fmt.Printf("arriving to %s @ %s)\n", stop_descs[origin], stops[i].arr.Format("15:04:05"))

			for i+=1; i<len(stops); i++ {
				//fmt.Printf("%-12s --> %-24s .... ", stop_descs[start.id], stop_descs[stops[i].id])
				s = stops[i]
				if _, ok := visited[s.id[:3]]; !ok {
					var id string
                    for _, id = range not_visited {
						if id == s.id[:3] { break }
					}
					if id != s.id[:3] {
						/* Create new node, add to not_visited */
                        not_visited = append(not_visited, s.id[:3])
                        /*
						fmt.Printf("Add %-24s (%s) to not_visited\n", 
						      stop_descs[s.id[:3]], s.id[:3])
                              */
					} else { }//fmt.Printf("%s already in not_visited\n", s.id) }
				} else {
					//fmt.Printf("%s already visited\n", s.id)
				}
			}
		}
        if len(lines) > 2 {
            nodes[origin] = make([]string, 0)
            for k, _ := range lines {
                nodes[origin] = append(nodes[origin], k)
            }
        }
	}
    return nodes
}

func navigate(origin, dest string) {
	if _, ok := transfers[origin]; ok {
		fmt.Println("Multiple lines available at origin")
	}
	

	/*
	nodes = make([]*Node, 1)
	nodes[0] = &Node{&Stop{origin, stop_descs[origin], 
			time.Now(), time.Now(), nil}, 0, nil}
	visited := make(map[string]struct{})
	//get_transfers(origin, nodes[0], visited)
	*/
    nodes := make(map[string]*Node)
    s = time.Now()
    for n, _ := range get_nodes(origin) {
    //for n, lines := range get_nodes(origin) {
        //fmt.Printf("%-12s (%s) is a platform transfer station serviced by: %s \n", stop_descs[n], n, lines)
        nodes[n] = New_Node(n)
    }
    for t, obj := range transfers {
        if stations[t] == nil { continue }
        lines := make([]string, 0)
        for _, l := range obj { lines = append(lines, l.dest) }
        //fmt.Printf("%-12s (%s) is a walking transfer station serviced by: %s \n", stop_descs[t], t, lines)
        nodes[t] = New_Node(t)
    }
    e = time.Now()
    fmt.Printf("%f seconds to enumerate all nodes\n", e.Sub(s).Seconds())

    nodes[origin] = New_Node(origin)
    nodes[dest] = New_Node(dest)

    s = time.Now()
	weights := do_Dijkstra(nodes, origin, dest)
    e = time.Now()
    fmt.Printf("%f seconds to do Dijkstra\n", e.Sub(s).Seconds())

    schedule := ""
    for cur := weights[dest]; cur != nil; cur = cur.prev {
        if cur.arr != nil {
            schedule = fmt.Sprintf("Take the %s-bound %s train @ %s to %s @ %s\n%s", 
                    stop_descs[cur.arr.trip.stops[len(cur.arr.trip.stops)-1].id], cur.arr.trip.route_id,
                    cur.arr.stop.arr.Format("15:04:05"), stop_descs[cur.station.id],
                    cur.time.Format("15:04:05"), schedule)
        } else {
            transfer_time := 0
            for _, t:= range transfers[cur.station.id] {
                if t.dest[:3] == cur.prev.station.id[:3] { transfer_time = t.time }
            }
            schedule = fmt.Sprintf("Transfer to %s (%d seconds)\n%s",
                stop_descs[cur.station.id], transfer_time, schedule)

        }
    }
    fmt.Println(schedule)

	if false {
		fmt.Printf("%s --> ", origin)
		fmt.Printf("%s via %s line to ", origin[0])
		fmt.Printf("%s\n", dest)
		return
	}
}

func trip_id_to_start_time(trip_id string) (time.Time, error) {
    frac_time_str, _, found := strings.Cut(trip_id, "_")
    if !found {
        return time.Now(), errors.New("No '_' found")
    }

    frac_time, _ := strconv.Atoi(frac_time_str)
    hour := frac_time / 100 / 60
    min := frac_time / 100 % 60
    sec := frac_time % 100 * 60 / 100 
    return time.Date(0, 0, 0, 
                    hour, min, sec, 0, nyc_loc), nil
}

func get_next_to_arrive(station_id, direction string) {
	if len(direction) < 1 {
		direction = "SN"
	}
	for _, d := range direction {
		if d == 'S' {
			fmt.Println("=== Southbound ===")
		} else if d == 'N' {
			fmt.Println("=== Northbound ===")
		}
		station, ok := stations[station_id+string(d)]
		if !ok {
			fmt.Printf("Station Not Found")
			return
		}
		if station.boarding != nil {
			fmt.Printf("0 min boarding now\n")
		}
		for _, arrival := range station.arrivals {
			fmt.Printf("%2d min at %s\n",
				int(arrival.stop.arr.Sub(time.Now()).Minutes()),
				arrival.stop.arr.Format("15:04:05 PM"))
		}
	}
}
func get_trains_by_line(line string) {
    client := &http.Client{}
    req, err := http.NewRequest("GET",
    "https://api-endpoint.mta.info/Dataservice/mtagtfsfeeds/nyct%2Fgtfs"+line, nil)
    req.Header.Add("x-api-key", "QgfDPBcfgE3M4uQIaogFr3wO7RN8xgWN6wo0fmNa")

    rsp, err := client.Do(req)
    if err != nil {
        fmt.Printf("%v", err)
        return
    }

    body, err := ioutil.ReadAll(rsp.Body)
    if err != nil {
        fmt.Printf("%v", err)
        return
    }


    feed := pb.FeedMessage{}
    err = proto.Unmarshal(body, &feed)
    if err != nil {
        fmt.Printf("%v", err)
        return
    }

    for _, ent := range feed.GetEntity() {

		var trip *Trip
		var pb_trip *pb.TripDescriptor
		var pb_stops []*pb.TripUpdate_StopTimeUpdate
		var pb_vehicle *pb.VehiclePosition
		var ok bool
        if pb_update := ent.GetTripUpdate(); pb_update != nil {
			//fmt.Println("========= Update  =========")
			trip, ok = trips[pb_update.GetTrip().GetTripId()]
			if !ok {
				pb_trip = pb_update.GetTrip()
			}
			pb_stops = pb_update.GetStopTimeUpdate()
        } else if pb_vehicle = ent.GetVehicle(); pb_vehicle != nil {
			//fmt.Println("========= Vehicle =========")
			
			trip, ok = trips[pb_vehicle.GetTrip().GetTripId()]
			if !ok {
				pb_trip = pb_vehicle.GetTrip()
			}
		} else {
			fmt.Println("========= Alert =========")
			continue
		}


		if !ok {
			new_trip := &Trip{}
			new_trip.New(pb_trip)
			//fmt.Printf("(line: %s) (new) %v\n", line, new_trip.String())
			trips[new_trip.trip_id] = new_trip
			trip = new_trip
		} else {
			//fmt.Printf("(line: %s) (exists %s)\n", line, trip.trip_id)
		}

		if pb_vehicle != nil {
			if station, ok := stations[pb_vehicle.GetStopId()]; ok {
				station.boarding = trip
			}
		}

		for _, pb_stop := range pb_stops {
			stop := Stop{}
			stop.New(pb_stop)

			station, ok := stations[stop.id]
			if !ok {
				new_station := Station{}
				new_station.New(&stop, stop.id[:3])
				stations[stop.id] = &new_station
				station = &new_station
			}

			new_arrival := Arrival{trip, &stop}
			station.InsertArrival(&new_arrival)
			
			trip.stops = append(trip.stops, stop)
		}
    }
    return
}

type Arrival struct {
	trip *Trip;
	stop *Stop;
}

type Station struct {
	id string;
	des string;
	arrivals []Arrival;
	boarding *Trip;
	parent *Station;
}

func (station *Station) New(stop *Stop, parent_id string) {
	station.id = stop.id
	station.des = stop.des
	var parent *Station
	var ok bool
	if parent, ok = stations[parent_id]; !ok {
		parent = &Station{parent_id, stop_descs[parent_id], nil, nil, nil}
		stations[parent_id] = parent
	}
	station.parent = parent

}

func (station *Station) InsertArrival(arr *Arrival) {
	if station.parent != nil {
		station.parent.InsertArrival(arr)
	}
	InsertArrival(arr, &station.arrivals)
}
func InsertArrival(arr *Arrival, p_arrivals *[]Arrival) {
    _InsertArrival(arr, p_arrivals)
    for ix, a := range *p_arrivals {
        if ix == 0 { continue }
        if ix == len(*p_arrivals) - 1 {
            if a.stop.arr.Before((*p_arrivals)[ix-1].stop.arr) {
                panic(a.trip.String() + a.stop.String())
            }
        } else {
            if a.stop.arr.Before((*p_arrivals)[ix-1].stop.arr) &&
                a.stop.arr.After((*p_arrivals)[ix+1].stop.arr) {
                panic(a.trip.String() + a.stop.String())
            }
        }
    }
}

func _InsertArrival(arr *Arrival, p_arrivals *[]Arrival) {
	arrivals := (*p_arrivals)[:]
	l, h := 0, len(arrivals)
	m := ((h - l) / 2) + l
	if h == 0 {
		arrivals = append(arrivals, *arr)
		*p_arrivals = arrivals[:]
		return
	}
	if h == 1 {
		if arr.stop.arr.Before(arrivals[0].stop.arr) {
			arrivals = append(arrivals, arrivals[0])
			arrivals[0] = *arr
		} else {
			arrivals = append(arrivals, *arr)
		}
		*p_arrivals = arrivals[:]
		return
	}

	for {
		if h - l < 2 {
			arrivals = append(arrivals, *arr)
			copy(arrivals[m+1:], arrivals[m:len(arrivals)-1])
			if arr.stop.arr.Before(arrivals[m].stop.arr) {
				arrivals[m] = *arr
			} else {
				arrivals[m] = arrivals[m+1]
				arrivals[m+1] = *arr
			}
			*p_arrivals = arrivals[:]
			return
		}
		if arr.stop.arr.Before(arrivals[m].stop.arr) {
			h = m
			m = ((h - l) / 2) + l
		} else {
			l = m
			m = ((h - l) / 2) + l
		}
	}
}

type Stop struct {
	id string;
	des string;
	arr time.Time;
	dep time.Time;
	transfers []Transfer;
}

func (stop *Stop) New(pb_stop *pb.TripUpdate_StopTimeUpdate) {
	stop.id  = pb_stop.GetStopId()
	stop.des = stop_descs[stop.id]
	stop.arr = time.Unix(pb_stop.GetArrival().GetTime(), 0)
	stop.dep = time.Unix(pb_stop.GetDeparture().GetTime(), 0)
	if ts, ok := transfers[stop.id[:3]]; ok {
		stop.transfers = ts
	}
}

func (stop *Stop) String() (string) {
	return fmt.Sprintf("id: %s,    des: %s,\tarr: %s,\tdep: %s\t",
			stop.id, stop.des, stop.arr.String(), stop.dep.String())
}


type Trip struct {
    trip_id string;
    route_id string;
    start_time time.Time;
	stops []Stop;
}

func (trip *Trip) New(pb_trip *pb.TripDescriptor) {
    trip.trip_id = pb_trip.GetTripId()
    trip.route_id = pb_trip.GetRouteId()
	start_time, _ := trip_id_to_start_time(pb_trip.GetTripId())
	start_date := pb_trip.GetStartDate()

	yr, _ := strconv.Atoi(start_date[:4])
	mon, _ := strconv.Atoi(start_date[4:6])
	day, _ := strconv.Atoi(start_date[6:])
    trip.start_time = start_time.AddDate(yr, mon, day)
}

func (trip *Trip) String() (string) {
	return fmt.Sprintf("trip_id: %s,\troute_id: %s,\tstart_time: %s,\tstops: %d\t",
		trip.trip_id, trip.route_id, trip.start_time.String(), len(trip.stops))
}

func coords2dist(lat1, long1, lat2, long2 float64) (dist float64) {
    //fmt.Printf("%f, %f\n %f, %f\n", lat1, long1, lat2, long2)
    d, _, _ := wgs.Inverse(
        lat1 / 180 * math.Pi,
        long1 / 180 * math.Pi,
        lat2 / 180 * math.Pi,
        long2 / 180 * math.Pi)
    return d
}
