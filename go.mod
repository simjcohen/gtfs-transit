module transit

go 1.18

require (
	github.com/mattn/go-sqlite3 v1.14.18
	google.golang.org/protobuf v1.31.0
)

require github.com/lvdlvd/go-geo-wgs84 v0.0.0-20151021055236-bc00bccc4004 // indirect
